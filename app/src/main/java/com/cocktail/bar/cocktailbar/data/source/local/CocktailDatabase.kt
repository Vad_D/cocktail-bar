package com.cocktail.bar.cocktailbar.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Cocktail::class, Ingredient::class], version = 1)
abstract class CocktailDatabase : RoomDatabase() {

    abstract fun getDao(): DatabaseDao
}
