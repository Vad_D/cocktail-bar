package com.cocktail.bar.cocktailbar.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cocktail.bar.cocktailbar.data.source.local.Cocktail
import com.cocktail.bar.cocktailbar.domain.SaveCocktailUseCase
import com.cocktail.bar.cocktailbar.util.Event
import com.cocktail.bar.cocktailbar.util.addElement
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class CreateCocktailViewModel @Inject constructor(
    private val saveCocktailUseCase: SaveCocktailUseCase
) : ViewModel() {

    val returnToCocktailScreenEvent = MutableLiveData<Event<Unit>>()
    val saveCocktailEvent = MutableLiveData<Event<Unit>>()

    val title = MutableLiveData<String?>()
    val description = MutableLiveData<String?>()
    val recipe = MutableLiveData<String?>()
    val ingredients = MutableLiveData<List<String>?>()


    fun onSaveCocktailClicked() {
        saveCocktailEvent.value = Event(Unit)

        if (!title.value.isNullOrBlank()) {
            val cocktail = Cocktail("", title.value!!, description.value ?: "", recipe.value ?: "")
            viewModelScope.launch {
                saveCocktailUseCase(cocktail)

                CocktailsViewModel.cocktails.addElement(cocktail)

                returnToCocktailScreenEvent.value = Event(Unit)
            }
        }
    }

    fun onCancelSaveCocktailClicked() {
        returnToCocktailScreenEvent.value = Event(Unit)
    }
}