package com.cocktail.bar.cocktailbar.di

import com.cocktail.bar.cocktailbar.data.source.local.DatabaseDao
import com.cocktail.bar.cocktailbar.domain.GetCocktailsUseCase
import com.cocktail.bar.cocktailbar.domain.SaveCocktailUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCasesModule {

    @Provides
    @Singleton
    fun provideGetCocktailsUseCase(databaseDao: DatabaseDao): GetCocktailsUseCase {
        return GetCocktailsUseCase(databaseDao)
    }


    @Provides
    @Singleton
    fun provideSaveCocktailUseCase(databaseDao: DatabaseDao): SaveCocktailUseCase {
        return SaveCocktailUseCase(databaseDao)
    }
}