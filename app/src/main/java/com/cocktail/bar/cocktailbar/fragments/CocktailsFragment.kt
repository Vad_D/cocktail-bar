package com.cocktail.bar.cocktailbar.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.cocktail.bar.cocktailbar.R
import com.cocktail.bar.cocktailbar.adapters.recycler.CocktailsAdapter
import com.cocktail.bar.cocktailbar.databinding.DialogBottomSheetCocktailBinding
import com.cocktail.bar.cocktailbar.databinding.FragmentCocktailsBinding
import com.cocktail.bar.cocktailbar.util.EventObserver
import com.cocktail.bar.cocktailbar.util.setupBottomSheet
import com.cocktail.bar.cocktailbar.viewmodels.CocktailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CocktailsFragment : Fragment() {

    private var _binding: FragmentCocktailsBinding? = null
    private val viewModel by viewModels<CocktailsViewModel>()
    private lateinit var dialogBottomSheetDataBinding: DialogBottomSheetCocktailBinding

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        _binding = FragmentCocktailsBinding.inflate(inflater, container, false)
        dialogBottomSheetDataBinding =
            DialogBottomSheetCocktailBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = this.viewLifecycleOwner
        binding.viewModel = viewModel

        dialogBottomSheetDataBinding.lifecycleOwner = this.viewLifecycleOwner
        dialogBottomSheetDataBinding.viewModel = viewModel

        setupListAdapter()
        setupListeners()

        setupBottomSheet(
            dialogBottomSheetView = dialogBottomSheetDataBinding.root,
            showBottomSheetEvent = viewModel.showBottomSheetEvent,
            hideBottomSheetEvent = viewModel.hideBottomSheetEvent
        )
    }

    private fun setupListeners() {
        viewModel.openCreateCocktailScreenEvent.observe(viewLifecycleOwner, EventObserver {
            findNavController().navigate(R.id.action_CocktFrag_to_CreateCocktFrag)
        })
    }

    private fun setupListAdapter() {
        val recyclerView = binding.recyclerViewCocktails
        val listAdapter = CocktailsAdapter(viewModel, recyclerView)
        recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        recyclerView.adapter = listAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}