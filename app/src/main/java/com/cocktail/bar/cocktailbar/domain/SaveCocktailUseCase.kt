package com.cocktail.bar.cocktailbar.domain

import com.cocktail.bar.cocktailbar.data.source.local.Cocktail
import com.cocktail.bar.cocktailbar.data.source.local.DatabaseDao

class SaveCocktailUseCase(private val databaseDao: DatabaseDao) {


    suspend operator fun invoke(cocktail: Cocktail) {

        databaseDao.addCocktail(cocktail)
    }
}