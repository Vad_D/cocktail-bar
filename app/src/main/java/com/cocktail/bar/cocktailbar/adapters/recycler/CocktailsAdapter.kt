package com.cocktail.bar.cocktailbar.adapters.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cocktail.bar.cocktailbar.data.source.local.Cocktail
import com.cocktail.bar.cocktailbar.databinding.ItemRecycerViewCocktailBinding
import com.cocktail.bar.cocktailbar.viewmodels.CocktailsViewModel


class CocktailsAdapter(
    private val viewModel: CocktailsViewModel,
    private val recyclerView: RecyclerView,
) : ListAdapter<Cocktail, CocktailsAdapter.ViewHolder>(TaskDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(private val binding: ItemRecycerViewCocktailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: CocktailsViewModel, item: Cocktail) {
            binding.viewModel = viewModel
            binding.cockt = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemRecycerViewCocktailBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class TaskDiffCallback : DiffUtil.ItemCallback<Cocktail>() {
    override fun areItemsTheSame(oldItem: Cocktail, newItem: Cocktail) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: Cocktail, newItem: Cocktail) = oldItem == newItem
}