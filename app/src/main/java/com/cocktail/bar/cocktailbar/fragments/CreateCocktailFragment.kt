package com.cocktail.bar.cocktailbar.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cocktail.bar.cocktailbar.databinding.FragmentCreateCocktailBinding
import com.cocktail.bar.cocktailbar.util.EventObserver
import com.cocktail.bar.cocktailbar.viewmodels.CreateCocktailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateCocktailFragment : Fragment() {

    private var _binding: FragmentCreateCocktailBinding? = null
    private val viewModel by viewModels<CreateCocktailViewModel>()


    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCreateCocktailBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = this.viewLifecycleOwner
        binding.viewModel = viewModel


        setupListeners()

    }

    private fun setupListeners() {
        viewModel.returnToCocktailScreenEvent.observe(viewLifecycleOwner, EventObserver {
            findNavController().popBackStack()
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}