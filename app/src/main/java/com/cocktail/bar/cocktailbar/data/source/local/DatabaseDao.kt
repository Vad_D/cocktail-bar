package com.cocktail.bar.cocktailbar.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface DatabaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCocktail(cocktail: Cocktail)

    @Query("SELECT * FROM cocktail_table")
    fun getCocktails(): Flow<List<Cocktail>>

}