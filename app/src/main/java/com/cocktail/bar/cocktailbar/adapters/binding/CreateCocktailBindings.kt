package com.cocktail.bar.cocktailbar.adapters.binding

import androidx.databinding.BindingAdapter
import com.cocktail.bar.cocktailbar.util.Event
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter(value = ["app:title", "app:saveTapedEvent"])
fun enableBottomSheetApplyChangesButton(
    inputLayout: TextInputLayout,
    title: String?,
    saveTapedEvent: Event<Unit>?,
) {
    if (title.isNullOrBlank()) {
        if (saveTapedEvent?.hasBeenHandled != null) inputLayout.error = "Fill in title"
    } else {
        inputLayout.error = null
    }
}