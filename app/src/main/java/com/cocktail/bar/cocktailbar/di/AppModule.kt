package com.cocktail.bar.cocktailbar.di

import android.content.Context
import androidx.room.Room
import com.cocktail.bar.cocktailbar.data.source.local.CocktailDatabase
import com.cocktail.bar.cocktailbar.data.source.local.DatabaseDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): CocktailDatabase {
        return Room.databaseBuilder(
            appContext,
            CocktailDatabase::class.java,
            "cocktails.db"
        ).build()
    }

    @Provides
    fun provideLogDao(database: CocktailDatabase): DatabaseDao {
        return database.getDao()
    }

    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO
}