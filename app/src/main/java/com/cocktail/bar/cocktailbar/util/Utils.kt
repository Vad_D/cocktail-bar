package com.cocktail.bar.cocktailbar.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog


fun <A> MutableLiveData<MutableList<A>>.addElement(element: A, index: Int? = null) {
    val updatedList = this.value?.toMutableList()
    if (index == null) updatedList?.add(element) else updatedList?.add(index, element)
    this.value = updatedList
}

fun Fragment.setupBottomSheet(
    dialogBottomSheetView: View,
    showBottomSheetEvent: LiveData<Event<Unit>>,
    hideBottomSheetEvent: LiveData<Event<Unit>>
) {
    val bottomSheetDialog = BottomSheetDialog(requireContext())
    bottomSheetDialog.apply {
        setContentView(dialogBottomSheetView)
        dismissWithAnimation = true
    }
    val bottomSheetBehavior = bottomSheetDialog.behavior

    bottomSheetBehavior.addBottomSheetCallback(object :
        BottomSheetBehavior.BottomSheetCallback() {
        var isSettlingUp = false

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            when (newState) {
                BottomSheetBehavior.STATE_DRAGGING -> activity?.hideKeyboard()
                BottomSheetBehavior.STATE_HIDDEN -> {
                    activity?.hideKeyboard()
                    isSettlingUp = true
                }

                else -> {}
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            if (isSettlingUp) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                isSettlingUp = false
            }
        }
    })

    showBottomSheetEvent.observe(viewLifecycleOwner, EventObserver {
        activity?.hideKeyboard()
        bottomSheetDialog.show()
    })

    hideBottomSheetEvent.observe(viewLifecycleOwner, EventObserver {
        bottomSheetDialog.dismiss()
    })
}

fun Activity.hideKeyboard() =
    currentFocus?.let { view ->
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(view.windowToken, 0)
    }