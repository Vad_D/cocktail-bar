package com.cocktail.bar.cocktailbar.adapters.binding

import android.widget.TextView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cocktail.bar.cocktailbar.adapters.recycler.CocktailsAdapter
import com.cocktail.bar.cocktailbar.data.source.local.Cocktail

@BindingAdapter(value = ["app:cocktailItems"])
fun setRecyclerItems(recyclerView: RecyclerView, tasksList: List<Cocktail>?) {
    tasksList?.let {
        (recyclerView.adapter as CocktailsAdapter).submitList(it)
    }
}

@BindingAdapter(value = ["app:visibility"])
fun setVisibility(
    textView: TextView,
    text: String?
) {
    textView.isVisible = !text.isNullOrEmpty()
}