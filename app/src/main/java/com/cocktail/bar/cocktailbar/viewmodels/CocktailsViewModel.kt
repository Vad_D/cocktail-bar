package com.cocktail.bar.cocktailbar.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.cocktail.bar.cocktailbar.data.source.local.Cocktail
import com.cocktail.bar.cocktailbar.domain.GetCocktailsUseCase
import com.cocktail.bar.cocktailbar.util.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch


@HiltViewModel
class CocktailsViewModel @Inject constructor(
    private val getCocktailsUseCase: GetCocktailsUseCase
) : ViewModel() {

    companion object {
        val cocktails = MutableLiveData<MutableList<Cocktail>>(mutableListOf())
    }

    val hideBottomSheetEvent = MutableLiveData<Event<Unit>>()
    val showBottomSheetEvent = MutableLiveData<Event<Unit>>()
    val cocktailClicked = MutableLiveData<Cocktail?>()

    val noCocktails: LiveData<Boolean> = cocktails.map { it.isEmpty() }

    val openCreateCocktailScreenEvent = MutableLiveData<Event<Unit>>()

    init {
        getCocktails()
    }

    private fun getCocktails() {

        viewModelScope.launch {
            cocktails.value = getCocktailsUseCase().first() as MutableList<Cocktail>
        }
    }

    fun onFabClicked() {
        openCreateCocktailScreenEvent.value = Event(Unit)
    }

    fun onRecyclerViewItemClicked(cocktail: Cocktail) {
        cocktailClicked.value = cocktail

        showBottomSheetEvent.value = Event(Unit)
    }


    fun onEditCocktailClicked() {

    }
}