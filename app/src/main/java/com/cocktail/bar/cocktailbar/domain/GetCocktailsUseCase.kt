package com.cocktail.bar.cocktailbar.domain

import com.cocktail.bar.cocktailbar.data.source.local.Cocktail
import com.cocktail.bar.cocktailbar.data.source.local.DatabaseDao
import kotlinx.coroutines.flow.Flow

class GetCocktailsUseCase(private val databaseDao: DatabaseDao) {

    operator fun invoke(): Flow<List<Cocktail>> {
        return databaseDao.getCocktails()
    }
}