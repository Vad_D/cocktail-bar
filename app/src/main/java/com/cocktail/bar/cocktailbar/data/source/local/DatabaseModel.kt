package com.cocktail.bar.cocktailbar.data.source.local

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.UUID

@Entity(tableName = "cocktail_table")
data class Cocktail(
    @ColumnInfo(name = "path")
    var imgPath: String,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "description")
    var description: String,

    @ColumnInfo(name = "recipe")
    var recipe: String,

    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: String = UUID.randomUUID().toString()
)


@Entity(
    tableName = "ingredient_table", foreignKeys = [ForeignKey(
        entity = Cocktail::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("cocktailId"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class Ingredient(
    @ColumnInfo(name = "cocktailId")
    var cocktailId: Int,

    @ColumnInfo(name = "name")
    var name: String,

    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: String = UUID.randomUUID().toString()
)

data class CocktailAndIngredients(
    @Embedded
    var cocktail: Cocktail,
    @Relation(
        parentColumn = "id",
        entityColumn = "cocktailId"
    )
    var ingredient: List<Ingredient>
)